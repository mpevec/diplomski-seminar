# Korištenje Eclipse Kura platforme s primjenom na Raspberry Pi modulu u svrhu integracije sa SymbIoTe sustavom

## Sažetak

Potrebno je istražiti mogućnosti Eclipse Kura platforme, instaliranje potrebne aplikacijske podrške za korištenje Eclipse Kura platforme na Raspberry Pi uređaju te povezati postavljeno sučelje sa SymbIoTe oblakom (eng. *SymbIote Cloud*) i njegovim suštinskim dijelom (eng. *SymbIoTe Core*). 

Eclipse Kura je softver koji radi na krajevima IoT mreže te služi kao sučelje (eng. *gateway*) preko kojeg se istoj mreži pristupa. Kura može vrlo lako pristupiti hardverskom dijelu računala na koje je postavljena, što omogućava mogućnost izrade raznih IoT aplikacija. S druge strane, SymbIoTe je sustav koji povezuje IoT platforme različitog tipa u jednu cjelinu. Uz to, SymbIoTe pruža programsko sučelje visoke razine (eng. *high-level API*) kako bi se tim sustavima moglo virtualno upravljati. 

Integracija Eclipse Kure i sustava SymbIote je važna jer bi se tako omogućilo aplikacijama spojenima na Eclipse Kuru da komuniciraju s drugim IoT sučeljima i platformama.